from django.urls import path
from .views import AdultsView, ChildrenView, ParentFeePrintView

app_name = 'montedb'

urlpatterns = [
    path('children/', ChildrenView.as_view(), name='child-list'),
    path('adults/', AdultsView.as_view(), name='adult-list'),
    path('fee/<int:pk>/', ParentFeePrintView.as_view(), name='parental-contribution')
]