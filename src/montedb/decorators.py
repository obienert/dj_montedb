from deepdiff import DeepDiff
from montedb.models import Report


def process_report_response(function):
    def wrap(request, *args, **kwargs):
        response = function(request, *args, **kwargs)
        data = response.context_data['data']
        meta = response.context_data['meta']
        person = meta['person']
        report_type = meta['type']
        diff = True
        reports = Report.objects.filter(person=person, type=report_type).order_by('-date')
        if reports:
            diff = DeepDiff(data, reports[0].data, ignore_order=True) != {}
        if diff:
            # Either the report has changed or no previous report has been found:
            # Store the current report into database
            report = Report(person=person, type=report_type, data=data)
            report.save()
        else:
            report = reports[0]
        response.context_data['data'] = report.data
        response.context_data['valid_from'] = report.valid_from
        response.context_data['created'] = report.date
        return response
    return wrap
