import functools
import ssl
from os.path import join

from django.conf import settings
from django.templatetags.static import static
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django_tables2 import SingleTableView
from django_weasyprint import WeasyTemplateResponse, WeasyTemplateResponseMixin
from django_weasyprint.utils import django_url_fetcher

from config.utils import get_reports
from .decorators import process_report_response
from .fees import Fee
from .models import Adult, Child
from .tables import AdultTable, ChildTable


class ChildrenView(SingleTableView):
    template_name = "montedb/child_list.html"
    table_class = ChildTable
    model = Child

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ChildrenView, self).get_context_data(**kwargs)
        context['children'] = 'active'
        context['reports'] = get_reports()
        return context


class AdultsView(SingleTableView):
    template_name = "montedb/adult_list.html"
    table_class = AdultTable
    model = Adult

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AdultsView, self).get_context_data(**kwargs)
        context['adults'] = 'active'
        context['reports'] = get_reports()
        return context


@method_decorator(process_report_response, name='dispatch')
class ParentFeeView(TemplateView):
    template_name = "montedb/parent_fee.html"

    def get_context_data(self, **kwargs):
        adult = Adult.objects.get(pk=kwargs['pk'])
        context = super(ParentFeeView, self).get_context_data(**kwargs)

        fee = Fee(adult)
        data = {
            'payer': fee.payer,
            'adults': fee.adult_calculations,
            'children': fee.children_calculations
        }
        meta = {
            'person': fee.person,
            'type': 'parental_contribution'
        }
        context['data'] = data
        context['meta'] = meta
        return context


class CustomWeasyTemplateResponse(WeasyTemplateResponse):
    # customized response class to change the default URL fetcher
    def get_url_fetcher(self):
        # disable host and certificate check
        context = ssl.create_default_context()
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        return functools.partial(django_url_fetcher, ssl_context=context)


class ParentFeePrintView(WeasyTemplateResponseMixin, ParentFeeView):
    # output of MyModelView rendered as PDF with hardcoded CSS
    pdf_stylesheets = [
        join(settings.BASE_DIR, static('site/css/fee.css').lstrip('/')),
    ]
    #
    # show pdf in-line (default: True, show download dialog)
    pdf_attachment = False
    # custom response class to configure url-fetcher
    response_class = CustomWeasyTemplateResponse
