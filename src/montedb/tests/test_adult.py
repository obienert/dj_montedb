from datetime import date, datetime

import pytest
from pytest_factoryboy import LazyFixture

from montedb.fees import Fee

currentDateTime = datetime.now()
current_year = currentDateTime.date().year


# noinspection PyTestParametrized
@pytest.mark.django_db
class TestAdult:

    def test_adult_model(self, adult, adult_2, adult_child, adult_child_2):
        """
        Test adult objects created by factory.
        """
        assert str(adult) == "Count Raven"
        assert len(adult.children.all()) == 1
        assert str(adult.children.first()) == "Lemmy Kilmister"
        assert str(adult.adultchild_set.first().kinship) == "father"

        assert str(adult_2) == "Amon Amarth"
        assert str(adult.partner.partner == adult)

    # noinspection PyTestParametrized
    @pytest.mark.parametrize("income__adult", [LazyFixture('adult')])
    def test_fee(self, adult, income, adult_child):
        """
        Low income, one parent, one child, 5 children entitled to child benefit.
        """
        fee = Fee(adult)
        assert income.adult == adult
        assert fee.children_calculations['Kilmister Lemmy']['fee'] == 58

    # noinspection PyTestParametrized
    @pytest.mark.parametrize("income__amount", [2000])
    @pytest.mark.parametrize("adult__household_size", [1])
    def test_fee_above_income_limit(self, adult, income, adult_child):
        """
        Income above limit. Maximal fee shall be issued.
        """

        fee = Fee(adult)
        assert income.adult == adult
        assert fee.children_calculations['Kilmister Lemmy']['fee'] == 217

    @pytest.mark.parametrize("income__amount", [790.83])
    @pytest.mark.parametrize("income_2__amount", [374.27])
    @pytest.mark.parametrize("adult__household_size", [2])
    @pytest.mark.parametrize("adult_2__household_size", [2])
    def test_low_income_earner(self, adult,
                               adult_2, income, income_2, adult_child, adult_child_2):
        """
        Low income earners (Below 1666,- € per month). No after-school-fee shall be be computed.
        """
        fee = Fee(adult)
        assert fee.children_calculations['Kilmister Lemmy']['fee'] == 73

    # noinspection PyTestParametrized
    @pytest.mark.parametrize("income__amount", [1609.99])
    @pytest.mark.parametrize("child__kita", [True])
    @pytest.mark.parametrize("adult__no_kiga_fee", [True])
    def test_fee_no_kita_fee(self, adult, income, child, adult_child):
        """
        At least one parent is on unemployment pay: Tick the 'No Kiga Fee' checkbox.
        Child is kiga kid. Expected result should be a fee of zero
        """
        fee = Fee(adult)
        assert income.adult == adult
        assert fee.children_calculations['Kilmister Lemmy']['fee'] == 0

    @pytest.mark.parametrize("income__adult", [LazyFixture('adult')])
    @pytest.mark.parametrize("income__amount", [2000])
    @pytest.mark.parametrize("child__kita", [True])
    @pytest.mark.parametrize("child__birth_date", [date(current_year-6, 8, 12)])
    def test_fee_last_kiga_year(self, adult, income, child, adult_child):
        """
        Kindergarten kid within last year before school.
        """
        fee = Fee(adult)
        assert child.birth_date == date(current_year-6, 8, 12)
        assert fee.children_calculations['Kilmister Lemmy']['fee'] == 0

    @pytest.mark.parametrize("income__adult", [LazyFixture('adult_3')])
    @pytest.mark.parametrize("income__amount", [2290.67])
    @pytest.mark.parametrize("adult_3__household_size", [3])
    def test_fee_average_earner(self, adult_3, income, adult_child_3, adult_child_4):
        """
        Average income, two children in school.
        """
        fee = Fee(adult_3)
        assert fee.children_calculations['Kilmister Lemmy']['fee'] == 150
        assert fee.children_calculations['Petterson Findus']['fee'] == 150

    @pytest.mark.parametrize("income__amount", [2262.96])
    @pytest.mark.parametrize("income_2__amount", [1191.27])
    @pytest.mark.parametrize("adult__household_size", [2])
    @pytest.mark.parametrize("adult_2__household_size", [2])
    @pytest.mark.parametrize("child__kita", [True])
    @pytest.mark.parametrize("child__birth_date", [date(current_year-4, 6, 22)])
    def test_fee_different_incomes(self, adult, adult_2, income, income_2, adult_child, adult_child_2):
        """
        Two parents with different average incomes, one more child in household, one child in kiga.
        """
        fee = Fee(adult)
        assert fee.children_calculations['Kilmister Lemmy']['fee'] == 94