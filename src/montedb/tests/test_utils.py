from deepdiff import DeepDiff

from config.utils import find_upper_neighbour, get_list_value_at_index_or_last


class TestUtils:

    def test_find_upper_neighbour(self, df_school_fee):
        lst = find_upper_neighbour(800, df_school_fee, 'income')
        assert lst[0] == 1000
        lst = find_upper_neighbour(1000, df_school_fee, 'income')
        assert lst[0] == 1000
        lst = find_upper_neighbour(1958, df_school_fee, 'income')
        assert lst[0] == 2000
        lst = find_upper_neighbour(6000, df_school_fee, 'income')
        assert lst[0] == 5800

    def test_get_list_value_at_index_or_last(self):
        lst = [1000, 100, 80, 60]
        assert get_list_value_at_index_or_last(lst, 1) == 100
        assert get_list_value_at_index_or_last(lst, 3) == 60
        assert get_list_value_at_index_or_last(lst, 5) == 60

    def test_diff_dicts(self):
        t1 = {1: 1, 2: 2, 3: 3, 4: {"a": "hello", "b": [1, 2, 3]}}
        t2 = {1: 1, 2: 2, 3: 3, 4: { "a": "hello", "b": [1, 3, 2, 3]}}
        t3 = {1: 1, 2: 2, 4: { "a": "hello", "b": [1, 3, 2, 3]}}
        diff = DeepDiff(t1, t2, ignore_order=True)
        assert diff == {}
        diff = DeepDiff(t1, t3, ignore_order=True, view='tree')
        item_removed = diff['dictionary_item_removed']
        changed = list(item_removed)[0]
        assert changed.t1 == 3

