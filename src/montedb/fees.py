import pandas as pd
from constance import config
from dateutil.relativedelta import relativedelta

from config.utils import find_upper_neighbour, get_list_value_at_index_or_last
from .models import Income, AdultChild
from django.conf import settings


class ChildCalculation:
    """Instance stores different values to be returned when fee calculation is done for that child"""
    income = None
    incomeApplied = None
    fee = None


class AdultCalculation:
    """
    Instance stores income values of known income types during fee calculation
    """
    salary = 0
    income = 0
    unemployment = 0
    other_payments = 0
    deduction = 0
    total_income = 0

    def set_income(self, income):
        income_type = income.type
        income_amount = income.amount
        if income_type == Income.SALARY:
            self.salary += income_amount
        elif income_type == Income.INCOME:
            self.income += income_amount
        elif income_type == Income.UNEMPLOYMENT:
            self.unemployment += income_amount
        elif income_type == Income.OTHER_PAYMENTS:
            self.other_payments += income_amount
        elif income_type == Income.DEDUCTION:
            self.deduction += -income_amount
        else:
            print("WARNING: Income type {} is not known for AdultCalculation!".format(income_type))


class Fee:
    """
    Provides methods to calculate parents fee for a given child"""

    def __init__(self, adult):
        """
        Sets up instance and starts fee calculation

        :return: None
        """
        self.total_income = 0
        self.lookup_column = 1
        self.no_kiga_fee = False
        self.adult_calculations = dict()
        self.children_calculations = dict()
        self.payer = None
        # Gets all adults to be taken into account
        self.adults, p = get_adults(adult)
        self.person = p.person_ptr
        self.payer = {
            'last_name': p.last_name,
            'first_name': p.first_name,
            'street': p.address.street,
            'house_number': p.address.house_number,
            'zip_code': p.address.zip_code,
            'city': p.address.city
        }

        # Gets all children to be taken into account
        self.children = get_children(adult)

        # Sums up total income
        self.calc_total_income()
        # Computes the column to be looked up in csv fee table
        self.calc_lookup_column()
        # Check if no_kiga_fee is set for any adult
        self.set_no_kiga_fee()
        # Calculates fee
        self.calc_fee()

    def calc_total_income(self):
        """
        For each given adult, add all income values to one total

        :return: None
        """
        for adult in self.adults:
            adult_calculation = AdultCalculation()
            adult_total_income = 0

            for income in adult.income.all():
                income_amount = income.amount
                if income.type == Income.DEDUCTION:
                    self.total_income -= income_amount
                    adult_total_income -= income_amount
                else:
                    self.total_income += income.amount
                    adult_total_income += income_amount
                adult_calculation.set_income(income)

            adult_calculation.total_income = adult_total_income
            self.adult_calculations[f"{adult.last_name} {adult.first_name}"] = adult_calculation.__dict__

    def calc_lookup_column(self):
        """
        Computes the column to be looked up for fee value by the number
        of children having the right to child benefit.

        :return: None
        """
        column = len(self.children)
        household_size = 0
        for adult in self.adults:
            size = adult.household_size
            try:
                household_size = max(household_size, size)
            except (ValueError, TypeError):
                pass
        self.lookup_column = max(household_size, column)

    def set_no_kiga_fee(self):
        """
        Set the instance parameter no_kiga_fee True if at least for one adult this is
        set to True.

        :return: None
        """
        for adult in self.adults:
            if adult.no_kiga_fee:
                self.no_kiga_fee = True
                break

    def calc_fee(self):
        due_date = config.SCHOOL_ENROLMENT_DUE_DAY
        for child in sorted(self.children, key=lambda x: x.birth_date):
            child_calculation = ChildCalculation()
            child_calculation.income = self.total_income
            # Check if the pupil will be age 6 on due day
            six_years_old_date = child.birth_date + relativedelta(years=+6)
            if child.kita and six_years_old_date < due_date:
                fee = 0
            else:
                fee = self.get_fee(child.kita, child.care_time)
            child_calculation.incomeApplied = self.total_income
            child_calculation.fee = fee
            self.children_calculations[f'{child.last_name} {child.first_name}'] = child_calculation.__dict__

    def get_fee(self, is_kita, care_time):
        """
        Looks up fee in panda data frames. Considers income limits.

        :param bool is_kita: If true, kita fees will be looked up
        :param int care_time: If >= 6, a different fee table will be used
        :return: fee
        :rtype: int
        """
        fee = 0
        filenames = []
        if not is_kita:
            filenames = [settings.FILE_NAMES['SCHOOL_FEE']]
            # After school fee will only be issued if income limit gets exceeded
            # and no_kiga_fee is False
            if self.total_income > config.INCOME_LIMIT and not self.no_kiga_fee:
                filenames.append(settings.FILE_NAMES['AFTER_SCHOOL_FEE'])
        else:
            # kindergarten fee will only be issued if income limit gets exceeded
            # and no_kiga_fee is False
            if self.total_income > config.INCOME_LIMIT and not self.no_kiga_fee :
                # TODO: Put that value into config
                if care_time <= 6:
                    filenames = [settings.FILE_NAMES['KIGA_LE_30H_FEE']]
                else:
                    filenames = [settings.FILE_NAMES['KIGA_GT_30H_FEE']]
        for filename in filenames:
            df = pd.read_csv(filename)
            upper_income_fees = find_upper_neighbour(self.total_income, df, 'income')
            fee += get_list_value_at_index_or_last(upper_income_fees, self.lookup_column)
        return fee


def get_adults(adult):
    """
    For each child of the given adult, find all other adults that have been marked liable
    to pay for that child. If no payer has been defined (Recipient of demand of payment),
    the first adult in the list of collected adults will become the payer (Recipient).

    :param Adult adult:
    :return: List of adults, payer
    :rtype: [Adult], Adult
    """
    adults = set()
    payer = None
    for child in adult.children.all():
        # Only adults that have been marked as liable to pay for that child
        for adult in child.adults.filter(adultchild__liable=True):
            adults.add(adult)
            adult_child = AdultChild.objects.get(adult=adult, child=child)
            if adult_child.payer:
                payer = adult
    if not payer:
        adult_list = list(adults)
        adult_list.sort(key=lambda x: (x.last_name, x.first_name))
        payer = adult_list[0]
    return adults, payer


def get_children(adult):
    """
    Collects all children for which the given adult is liable to pay.

    :param Adult adult: adult instance
    :return: Set of child instances
    :rtype: set
    """
    children = set()
    for child in adult.children.filter(adultchild__liable=True):
        children.add(child)
    return children


