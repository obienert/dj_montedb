from django.urls import reverse

from montedb.models import Report


def find_upper_neighbour(value, df, col_name):
    exact_match = df[df[col_name] == value]
    if not exact_match.empty:
        return exact_match.values[0]
    else:
        values = df[df[col_name] > value]
        upper_neighbour_ind = values[col_name].idxmin() if not values.empty else df[col_name].idxmax()
        res = list(df.iloc[upper_neighbour_ind])
        return res


def get_list_value_at_index_or_last(lst, i):
    return lst[i] if len(lst) > i else lst[-1]


def get_reports():
    reports = []
    for report in Report.objects.all()[:10]:
        view_from_type = report.type.replace("_", "-")
        reports.append({
            'type': report.get_type_display(),
            'name': f"{report.person.first_name} {report.person.last_name}",
            'date': report.date,
            'url': reverse(f'montedb:{view_from_type}', args=[report.person.adult.id])
        })
    return reports
