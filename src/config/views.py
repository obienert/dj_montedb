from django.views import generic
from django_ajax.mixin import AJAXMixin

from config.utils import get_reports


class HomePage(generic.TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(HomePage, self).get_context_data(**kwargs)
        context['home'] = 'active'
        return context

class Reports(AJAXMixin, generic.TemplateView):
    template_name = "reports.html"

    def get_context_data(self, **kwargs):
        context = super(Reports, self).get_context_data(**kwargs)
        context['reports'] = get_reports()
        return context
