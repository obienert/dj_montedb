(function () {
    $(window).scroll(function () {
        const top = $(document).scrollTop();
        $('.corporate-jumbo').css({
            'background-position': '0px -' + (top / 3).toFixed(2) + 'px'
        });
        if (top > 50)
            $('.navbar').removeClass('navbar-transparent');
        else
            $('.navbar').addClass('navbar-transparent');
    }).trigger('scroll');

    /*
    window.operateEvents = {
        'click .feebutton': function (e, value, row, index) {
            alert('You clicked fee action, row: ' + JSON.stringify(row))
        }
    }
    $('.feebutton').click(function () {
        const id = $(this).attr("data-id");
        $.ajax({
            type: "GET",
            url: "montedb/fee",
            data: {adult_id: id},
            success: function () {
                const id = $(`#fee${id}`);
                id.removeClass('btn btn-primary btn-sm');
                id.addClass('btn btn-success btn-sm');
            }
        })
    });*/

})();

const loadReports = () => {
    $.ajax({
        url: "/reports",
        context: document.body,
        success: function(content){
            $('div.reports').html(content.content)
        }
    });
};

loadReports();
setInterval(loadReports, 5000);

