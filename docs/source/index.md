% Monte-DB documentation master file, created by
% sphinx-quickstart on Thu Oct 21 14:23:48 2021.
% You can adapt this file completely to your liking, but it should at least
% contain the root `toctree` directive.

# Welcome to Monte-DB's documentation!

**Monte-DB** is a web application to help staff with school and kindergarten management.

Features included:

- Parental fee calculation

```{toctree} 
---
maxdepth: 3
caption: 'Contents:'
---

usage
api
```


## Indices and tables

- {ref}`genindex`
- {ref}`modindex`
- {ref}`search`
