# Usage

## Installation

Clone the project:

```{code-block} console

git clone git@github.com:oliverbienert/dj_montedb.git
cd dj_montedb
mkdir logs
mkdir media
cp src/config/settings/local.sample.env src/config/settings/local.env
```

## Quick start

To set up a development environment quickly, first install Python 3. It
comes with virtualenv built-in. So create a virtual env by:

```{code-block} console

python3 -m venv ~/.venvs/dj_montedb
~/.venvs/dj_montedb/bin/activate

```

Or alternatively on Linux Mint 20:

```{code-block} console

source ~/.venvs/dj_montedb/bin/activate
```
Install all dependencies:

```{code-block} console

pip install -r requirements.txt
```
On some Linux systems like Ubuntu, Pillow will not install unless you install a C compiler and dependencies:

```{code-block} console

sudo apt-get install python3-dev python3-setuptools libjpeg-dev zlib1g-dev libpq-dev
```
Run migrations:

```{code-block} console

cd src
./manage.py migrate
./manage.py createsuperuser
```
Run the development server:

```{code-block} console

./manage.py runserver
```