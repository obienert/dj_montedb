# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import django
django_version = ".".join(map(str, django.VERSION[0:2]))
python_version = ".".join(map(str, sys.version_info[0:2]))

sys.path.insert(0, os.path.abspath('../../src'))

os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings.development'
django.setup()


# -- Project information -----------------------------------------------------

project = 'Monte-DB'
copyright = '2021, Oliver Bienert'
author = 'Oliver Bienert'

# The full version, including alpha/beta/rc tags
release = '0.1'

# Auto-generate API documentation.
os.environ['SPHINX_APIDOC_OPTIONS'] = "members,undoc-members,show-inheritance"

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinxcontrib.apidoc',    # runs sphinx-apidoc automatically as part of sphinx-build
    'sphinx.ext.autodoc',      # the autodoc extensions uses files generated by apidoc
    'sphinxcontrib_django',    # does some nicer django autodoc formatting
    'sphinx.ext.viewcode',     # enable viewing autodoc'd code
    'sphinx.ext.intersphinx',  # make links between different sphinx-documented packages
    'myst_parser',             # This will activate the MyST Parser extension, causing all documents with the .md
                               # extension to be parsed as MyST.
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Extension configuration -------------------------------------------------

autodoc_member_order = 'bysource'
autodoc_inherit_docstrings = False

apidoc_module_dir = '../../src'
apidoc_output_dir = 'apidoc'
apidoc_excluded_paths = ['accounts', 'config', 'manage.py', 'montedb/migrations']
apidoc_separate_modules = True
apidoc_toc_file = False
apidoc_module_first = True
apidoc_extra_args = ['-f']

intersphinx_mapping = {
   'python': ('https://docs.python.org/{}'.format(python_version), None),
   'django': ('https://docs.djangoproject.com/en/{}/'.format(django_version),
              'https://docs.djangoproject.com/en/{}/_objects/'.format(django_version)),
   'djangorestframework-jsonapi': ('https://django-rest-framework-json-api.readthedocs.io/en/stable/',
                                   'https://django-rest-framework-json-api.readthedocs.io/en/stable/objects.inv')
}



